#!/bin/bash

INIT=false
MIGRATION=false
CREATEDB=true

for DOCKER_OPTION in "$@"
do
    case "$DOCKER_OPTION" in
    init )
        INIT=true
        shift
        ;;
    migration )
        MIGRATION=true
        shift
        ;;
    with-existing-db )
        CREATEDB=false
        shift
        ;;
    * )
        break
        ;;
  esac
done

APP_ENV=${APP_ENV:-dev}

init_project () {
    composer install --prefer-dist --no-progress --no-suggest --no-interaction
    chmod +x bin/console && sync
    php bin/console assets:install

    if $CREATEDB
    then
        php bin/console doctrine:schema:update -f
        php bin/console doctrine:fixtures:load -n
    fi
}

if $MIGRATION;
then
    php bin/console doctrine:migrations:migrate --no-interaction -vvv
    chown -R www-data:www-data /srv/api/var/
fi

if $INIT;
then
    mkdir -p /var/nginx/client_body_temp
    chown www-data:www-data /var/nginx/client_body_temp
    mkdir -p /var/run/php/
    chown www-data:www-data /var/run/php/
    touch /var/log/php-fpm.log
    chown www-data:www-data /var/log/php-fpm.log

    if [ "$APP_ENV" != 'prod' ];
    then
	    init_project
	fi
    exec supervisord --nodaemon --configuration="/etc/supervisord.conf" --loglevel=info
fi

exec "$@";
