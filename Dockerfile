FROM alpine:latest

COPY --from=library/docker:latest /usr/local/bin/docker /usr/bin/docker
COPY --from=docker/compose:latest /usr/local/bin/docker-compose /usr/bin/docker-compose

RUN adduser -S www-data -u 1000
COPY --from=nginx:latest /etc/nginx/nginx.conf /nginx.conf
RUN apk upgrade -U
RUN apk add --update --no-cache zip unzip
RUN apk add --update --no-cache nginx supervisor
RUN apk add --update --no-cache \
        git \
        bash \
        vim
RUN apk --update --no-cache add php7 php7-fpm php7-mysqli php7-json php7-openssl php7-curl\
    php7-zlib php7-xml php7-phar php7-intl php7-dom php7-xmlreader php7-ctype php7-session\
    php7-mbstring php7-gd curl

COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1
RUN composer global require "hirak/prestissimo:^0.3" --prefer-dist --no-progress --no-suggest --classmap-authoritative \
&& composer clear-cache
ENV PATH="${PATH}:/root/.composer/vendor/bin"

#COPY config/nginx.conf /etc/nginx/nginx.conf

RUN mkdir -p var/cache var/log var/sessions /var/www/html var/www/src

VOLUME /var/www/html
VOLUME /var/www/src

COPY index.php /var/www/html/

COPY src/run.sh /run.sh
RUN chmod u+rwx /run.sh

EXPOSE 80

ENTRYPOINT [ "/run.sh" ]
CMD ["init"]